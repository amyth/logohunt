#!/usr/bin/env python
# -*- coding: utf-8 -*-

#    @Package - LogoHunt (v 0.1)
#    @Module - logohunt.browser.browser
#    @Author - Amyth Arora <mail@amythsingh.com>
#
#    Copyright (C) 2014  Amyth Arora
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; Applies version 2 of the License.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA


import httplib
import socket
import urllib
import urllib2

import mssg
import settings

from decorators import retry
from user_agents import AGENTS
from exceptions import BrowserError

class HTTPConnection(httplib.HTTPConnection):
    """
    Extend the base httplib's HTTPConnection class.
    """

    def connect(self):
        """
        Initializes a HTTP connection to a given host and port
        """

        for res in socket.getaddrinfo(self.host, self.port, 0, socket.SOCK_STREAM):
            af, socktype, proto, canonname, sa = res
            try:
                self.sock = socket.socket(af, socktype, proto)
                self.sock.settimeout(settings.SOCKET_TIMEOUT)
                self.sock.connect(sa)
            except socket.error, mssg.SOCKET_ERROR:
                logger.warning("Connection Failed - %s:%s", self.host, self.port)
                if self.sock:
                    self.sock.close()
                self.sock = None
                continue
            break
        if not self.sock:
            raise socket.error, mssg.SOCKET_ERROR


class HTTPHandler(urllib2.HTTPHandler):
    """ This class extends the urllib2's base HTTPHandler class
    """

    def http_open(self, req):
        return self.do_open(HTTPConnection, req)


class Browser(object):
    """ This is the Browser object, That sets the headers to the request.
    """

    def __init__(self, user_agent=AGENTS[0], debug=False):
        self.headers = {
            'User-Agent': user_agent,
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Language': 'en-us,en;q=0.5'
        }
        self.debug = debug
        if settings.USE_PROXIES:
            self.proxies = []
            proxy_file = open(settings.PROXY_FILE_PATH, 'r').readlines()
            for proxy in proxy_file:
                self.proxies.append(proxy)

    @retry(Exception, tries=10, delay=1, backoff=1.3)
    def get_page(self, url, data=None):
        if settings.USE_PROXIES:
            if len(self.proxies):
                proxy = random.choice(self.proxies).strip()
                opener = urllib2.build_opener(urllib2.ProxyHandler(
                         {'http': 'http://' + proxy}))
            else:
                logger.critical("USE_PROXIES=True but no alive proxies!")
                sys.exit(-1)
        else:
            handlers = [HTTPHandler]
            opener = urllib2.build_opener(*handlers)
        if data:
            data = urllib.urlencode(data)
        request = urllib2.Request(url, data, self.headers)

        try:
            response = opener.open(request, timeout=settings.SOCKET_TIMEOUT)
            return response.read()
        except Exception, e:
            self.proxies.remove
            raise BrowserError(url, str(e))

    def set_random_user_agent(self):
        self.headers['User-Agent'] = random.choice(AGENTS)
        return self.headers['User-Agent']
