class BrowserError(Exception):
    """
    An Exception object that is raised when a
    browser error is encountered.
    """

    def __init__(self, url, error):
        self.url = url
        self.error = error
