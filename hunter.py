#!/usr/bin/env python
# -*- coding: utf-8 -*-

#    @Package - LogoHunt (v 0.1)
#    @Module - logohunt.hunter
#    @Author - Amyth Arora <mail@amythsingh.com>
#
#    Copyright (C) 2014  Amyth Arora
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; Applies version 2 of the License.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

import os
import random
import string
import urllib

from BeautifulSoup import BeautifulSoup

from browser import browser

class Hunter(object):
    """
    Searches logos over the internet for the given
    set/list of companies.
    """

    def __init__(self, names=None, download=False, download_dir=None):
        """
        Initialize and set the default options for
        the Hunter object
        """

        if isinstance(names, list) or isinstance(names, tuple):
            self.companies = list(names)
        elif isinstance(names, str) or isinstance(names, unicode):
            self.companies = [names]
        else:
            self.companies = []

        self.ghost_images = 0
        self.browser = browser.Browser()
        self.google_domain = "https://www.google.com/"
        self.result_depth = 1
        self.cnblack_list = ['pvt', 'private', 'ltd',
                'co', 'llc', 'corp', 'inc', 'limited']
        self.logos = {}
        self.downloaded = {}
        self.download = download
        self.download_dir = os.path.abspath(os.curdir)
        if download_dir:
            self.download_dir = os.path.abspath(download_dir)

    def _clean_google_href(self, href):
        href = href.split("?q=")[1]
        href = href.split("&")[0]
        return href

    def _create_ei_code(self):
        length = random.randint(19, 22)
        code = ''.join(random.choice(
            string.ascii_uppercase + string.ascii_lowercase
        ) for _ in range(15))

        return code

    def _create_linkedin_search_query(self, name):
        name = name.replace(" ", "+")
        query = "linkedin+company+%s" % name
        return query

    def _create_search_url(self, query):
        ei_code = self._create_ei_code()
        url = "%ssearch?hl=en-IN&source=hp&q=%s&gws_rd=ssl" % (
                self.google_domain, query)
        return url

    def _is_considerable(self, company, url, info):
        start_url = "linkedin.com/company/"
        company_url = start_url in url

        name_split = company.split(" ")
        clean_split = [x.lower() for x in name_split if (x.replace(".", "").replace(

            ",", "").lower()) not in self.cnblack_list]
        req_matches = len(clean_split)
        url_matches = 0
        info_matches = 0

        for part in clean_split:
            if part in url:
                url_matches += 1
            if part in info:
                info_matches += 1

        url_has_name = url_matches == req_matches
        info_has_name = info_matches == req_matches

        if company_url and (url_has_name or info_has_name):
            return True

        return False

    def _process_html(self, company, html):
        soup = BeautifulSoup(html)
        results = soup.findAll('li', {'class': 'g'})
        results = results[:self.result_depth]
        if not results:
            print "No results found for company '%s'" % company
            return

        for result in results:
            anchor = result.find('a')
            href = self._clean_google_href(anchor.get('href'))
            info_div = result.find('div')
            info = result.text

            considerable = self._is_considerable(company, href, info)
            if considerable:
                logo_url = self.fetch_logo_url(company, href)
                if logo_url:
                    self.logos[company] = logo_url

                    if self.download:
                        path = self.download_logo(company, logo_url)
                        if path:
                        self.downloaded[company] = path

    def download_logo(self, company, url):
        if "ghost_company" in url:
            self.ghost_images += 1
            return None

        ext = "." + url.split(".")[-1]
        image_name = company + ext
        path = os.path.join(self.download_dir, image_name)
        urllib.urlretrieve(url, path)
        print "Downloaded logo for '%s' and saved to %s" % (company, path)

        return path

    def fetch_logo_url(self, company, page_url):
        page = self.browser.get_page(page_url)
        soup = BeautifulSoup(page)
        img_container = soup.find('div', {'class': 'image-wrapper'})
        if img_container:
            image = img_container.find('img')
            return image.get('src')

        print "No image found for company '%s'" % company
        return None

    def get_company_logo(self, company):
        query = self._create_linkedin_search_query(company)
        surl = self._create_search_url(query)
        search_result = self.browser.get_page(surl)
        self._process_html(company, search_result)

        return search_result

    def get_logos(self):
        if not self.companies:
            print "No companies found. Exiting .. "
            return

        for company in self.companies:
            if not company in self.logos:
                self.get_company_logo(company)

        print "\n\n"
        print "Downloaded %d images for companies."\
              " Skipped %d ghost company images." % (len(self.downloaded.items()),
                      self.ghost_images)
