from functools import wraps
import time


def retry(ExceptionToCheck, tries=4, delay=3, backoff=2, logger=None):
    """
    Retry calling the decorated function using exponential backoff.

    ExceptionToCheck: the exception to check for.
                      May be a tuple of exceptions to check.
    tries: number of times to try (not retry) before giving up.
    delay: initial delay between retries in seconds.
    backoff: backoff multiplier e.g. value of 2 will double the delay
             each retry
    logger: logger to use.
    """

    def deco_retry(f):

        @wraps(f)
        def f_retry(*args, **kwargs):
            mtries, mdelay = tries, delay
            while mtries > 1:
                try:
                    return f(*args, **kwargs)
                except ExceptionToCheck, e:
                    msg = "%s, Retrying in %d seconds..." % (str(e), mdelay)
                    if logger:
                        logger.warning(msg)
                    time.sleep(mdelay)
                    mtries -= 1
                    mdelay *= backoff
            return f(*args, **kwargs)

        return f_retry  # true decorator

    return deco_retry
