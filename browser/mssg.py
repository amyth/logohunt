#!/usr/bin/python

SOCKET_ERROR = "Empty list found by getaddrinfo"
SOCKET_TIMEOUT_ERROR = "Request Time Out"
UNKNOWN_ERROR = "An Unknow Error Occured"
WRONG_INTERVAL_ERROR = "Wrong interval parameter '%s' passed to first_indexed_in_previous. Expected 'day', 'week', 'month', 'year' or number of months"
TITLE_TAG_NOT_FOUND = "Title tag in Google search result was not found"
DESC_TAG_NOT_FOUND = "Description tag in Google search result was not found"
PROXY_ERROR = "Error establishing connection. Check Proxy."
BAD_PROXY = "Bad Proxy, Trying Another Proxy ..."
